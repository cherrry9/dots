#                  ██      ██          ██
#                 ░██     ░██         ░██
#   ██████ ██   ██░██     ░██  ██     ░██
#  ██░░░░ ░░██ ██ ░██████ ░██ ██   ██████
# ░░█████  ░░███  ░██░░░██░████   ██░░░██
#  ░░░░░██  ██░██ ░██  ░██░██░██ ░██  ░██
#  ██████  ██ ░░██░██  ░██░██░░██░░██████
# ░░░░░░  ░░   ░░ ░░   ░░ ░░  ░░  ░░░░░░

# STATE/FLAGS {{{
# switch the node state on tiled - floating
super + t
  bspc query --nodes -n focused.tiled && state=floating || state=tiled; \
  bspc node --state ~$state

# switch the node state on tiled - pseudo tiled
super + p
  bspc node --state ~pseudo_tiled

# switch the node state on tiled - fullscreen
super + f
  bspc node --state ~fullscreen

# set marked,locked,sticky,private flag
super + {m,x,s,z}
  bspc node -g {marked,locked,sticky,private}

# hide / unhide the scratchpad
super + F1
  id=$(head -1 /tmp/scratchid);\
  bspc node $id --flag hidden;bspc node -f $id

# hide / unhide the currently active node
super + {_,shift +} u
  bsp-toggle {hide,show}

# hide / unhide the currently active node
super + {_,shift +} w
  {bsp-hidden,bspc node -g sticky; bspc node -g hidden}
# }}}

# MOVE/RESIZE {{{
# focus/swap the node in the given direction
#super + {_,shift + }{h,j,k,l}
#  bspc node -{f,s} {west,south,north,east}

# move a floating node
#alt + {h,j,k,l}
#  bspc node -v {-20 0,0 20,0 -20,20 0}

# change the node focus
super + {h,j,k,l}
  bspc node -f {west,south,north,east}

# swap the tiled node or move the floating node
super + shift + {h,j,k,l}
  bsp-move {west,south,north,east}

# focus/Swap the last node/desktop
super + {grave,shift + grave,apostrophe}
  bspc {node -f last.local,desktop -f last,node --swap last}

# focus or send the node to the given desktop
super + {_,shift +}g
  bspc node {-f,-s} biggest.local

# focus the next/previous node in the current desktop
super + {_,shift + }n
  bspc node -f {next,prev}.local

# expand a window by moving one of its side outward
#ctrl + super + {h,j,k,l}
#  bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# Contract a window by moving one of its side inward
#ctrl + super + shift + {h,j,k,l}
#  bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# resize the node
ctrl + super + {h,j,k,l}
  bsp-resize {west,south,north,east}

# resize gaps
ctrl + alt + g: {k,j}
  bspc config -d focused window_gap "$(($(bspc config -d focused window_gap) {-,+} 5 ))"
# }}}

# DESKTOP {{{
# focus the next/previous desktop
super + {_,shift + }Tab
  bspc desktop --focus {next,prev}.occupied

# move/Send the node to selected desktop
super + {_,shift + }{1-9,0}
  bspc {desktop -f,node -d} '^{1-9,10}'

# move the node to selected desktop
#alt + shift + {1-9,0}
#  swapdesktop {1-9,10}

# swap nodes betwen desktops
#alt + {1-9,0}
#  bspc node --to-desktop ^{1-9,10} --focus

# move node to the left, rigt desktop
#ctrl + shift + {h,l}
#    bspc node --to-desktop {prev,next} --focus
# }}}

# LAYOUTS {{{
# flip the layout vertically/horizontally
super + {v,b}
  bspc node @/ --flip {vertical,horizontal}

# rotate nodes tree
super + {_,shift +} r
   bspc node @/ -R {90,270}

# circulate the leaves of the tree
super + {_,shift + } c
  bspc node @/ --circulate {forward,backward}

# enable/Disable the monocle layout
super + {_,shift + }space
  bspc desktop --layout {next,prev}

# make the split ratios equal/balanced
super + shift + {e,b}
  bspc node @/ {--equalize,--balance}
# }}}

# PRESELECTION {{{
# make a new split
alt + shift + {h,j,k,l}
  bspc node -p {west,south,north,east}
super + alt + {p,b,comma,period}
  bspc node --focus @{parent,brother,first,second}

# cancel the preselection
super + {_,shift +} e
  {bspc node -p cancel,bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel}

# preselect the ratio
super + ctrl + {1-9}
  bspc node -o 0.{1-9}

# send the focused node to the newest preselected node
super + y
  bspc node focused -n newest.!automatic.local

# send the newest marked node to the newest preselected node
super + shift + y
  bspc node newest.marked -n newest.!automatic.local
# }}}

# APPLICATION {{{{
# close/Kill the node
super + {_,shift + }q
  bspc node -{c,k}
# run the dmenu desktop
super + d
  $DMENU_RUN
# open the terminal
super + Return
  $TERMINAL
# open the floating terminal
#aLt + Return
#  $TERMINAL -name float
# open the terminal in the same directory
super + shift + Return
  samedir
# open the file manager
alt + f
  $TERMINAL -e $FILE
# open the browser
alt + w
  $BROWSER
# open the mpd client
alt + m
  $TERMINAL -e ncmpcpp
# open the neovim
alt + v
  $TERMINAL -e nvim
# open the sound configurations
alt + a
  $TERMINAL -e alsamixer
# open the mail client
alt + e
  $TERMINAL -e neomutt
# show help
super + F2
  $TERMINAL -n float -e sxhkd-help
# }}}

# BACKLIGHT/AUDIO {{{
# increase the volume (mpd or alsa)
super + {equal,shift + plus}
  {volume alsa up,volume mpc up}
# decrease the volume (mpd or alsa)
super {_,shift} + minus
  {volume alsa down,volume mpc down}

# mute the sound
super + shift + m
  amixer sset Master toggle

# pause mpd audio
super + shift + p
  mpc toggle
# changes to the next/previous tracks
super + {comma,period}
  mpc {prev; dunstify "$(mpc current)",next; dunstify "$(mpc current)"}
# restart track
super + shift + less
  mpc seek 0%
# seek foward in song
super + {_,shift +} bracketright
  mpc seek +{10,120}
# seek backward in song
super + {_,shift +} bracketleft
  mpc seek -{10,120}

# increase the backlight
alt + {_,shift +} minus
  {brightness down, sudo -A keybacklight -dec}
# decrease the backlight
alt  + {_,shift +} equal
  {brightness up, sudo -A keybacklight -inc}
{XF86MonBrightnessDown,XF86MonBrightnessUp}
  {brightness down,brightness up}
# }}}

# SCRIPTS {{{
# bookmarks
super + ctrl + o
  bookmarks
# passmenu
super + ctrl + p
  passmenu2 --type
# show the unicode symbols
super + ctrl + i
  symbols
# lock, shutdown, reboot, exit?
super + Escape
  power
# take a screenshot
super + End
  screenshot
# record the screen
super + ctrl + Insert
  blaze
# kill the recording
super + ctrl + w
  blaze -s
# mount, umount a device
super + ctrl + d
  ANS=$(printf "mount\numount" | $DMENU -p 'Device'); case "$ANS" in mount) mountdev;; umount) umountdev;; esac
# display the information
super + slash
  notify-send \
  "$(date "+%a %d %b - %I:%M %p")" \
  "Desktop: $(bspc query -D -d focused --names)\n\
  Volume: $(amixer -D pulse get Master | grep -o '[0-9][0-9]*%' | head -1)\n\
  $(acpi)"
# show the clicboard content
super + Insert
  showclip
# update OS
super + ctrl + u
  update
# network menu
super + ctrl + n
  iwd
# open terminal if desktop is empty
~button1
  click-desktop
# }}}

# MEDIA KEYS {{{
# audiokeys
XF86AudioMute
  amixer sset Master toggle
XF86Audio{Raise,Lower}Volume
  volume {up,down}
XF86Audio{Next,Prev}
  mpc {next,prev}
XF86Audio{Pause,Play,Stop}
  mpc {pause,play,stop}
XF86Audio{Rewind,Forward}
  mpc seek {-,+}10
XF86AudioMedia
  $TERMINAL -e ncmpcpp
# }}}
